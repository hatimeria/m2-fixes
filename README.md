# Magento2 Fixes

This module contains a series of small fixes for the Magento2 platform

## Installation

Install the latest version with

```bash
$ composer config repositories.m2fixes git git@bitbucket.org:hatimeria/m2-fixes.git
$ composer require hatimeria/m2-fixes dev-master
```

##List of improvements and fixes

* Fix saving multi-select field in forms handled by UI components (magento issue #7723: https://github.com/magento/magento2/issues/7723)

### Author

Hatimeria - <http://www.hatimeria.com/>
List of contributors:

* Artur Jewuła <artur.jewula@hatimeria.pl>